<?php

use Illuminate\Database\Seeder;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            'title' => 'Ledeno Doba',
            'author_id' => 1,
            'year' => new \DateTime('2016-02-11'),
            'language' => 'SR',
            'original_language' => 'EN',
            'created_at' => new \DateTime('now'),
            'updated_at' => new \DateTime('now'),
        ]);
        DB::table('books')->insert([
            'title' => 'Dinosaurusi',
            'author_id' => 1,
            'year' => new \DateTime('2016-01-15'),
            'language' => 'SR',
            'original_language' => 'EN',
            'created_at' => new \DateTime('now'),
            'updated_at' => new \DateTime('now'),
        ]);
        DB::table('books')->insert([
            'title' => 'Mračna Kula',
            'author_id' => 2,
            'year' => new \DateTime('2015-06-21'),
            'language' => 'SR',
            'original_language' => 'EN',
            'created_at' => new \DateTime('now'),
            'updated_at' => new \DateTime('now'),
        ]);
        DB::table('books')->insert([
            'title' => 'Talisman',
            'author_id' => 2,
            'year' => new \DateTime('2015-08-30'),
            'language' => 'SR',
            'original_language' => 'EN',
            'created_at' => new \DateTime('now'),
            'updated_at' => new \DateTime('now'),
        ]);
    }
}
