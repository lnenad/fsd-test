<?php

use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert([
            'name' => 'Džošua Džordž',
            'created_at' => new \DateTime('now'),
            'updated_at' => new \DateTime('now'),
        ]);
        DB::table('authors')->insert([
            'name' => 'Stiven King',
            'created_at' => new \DateTime('now'),
            'updated_at' => new \DateTime('now'),
        ]);
    }
}
