<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'test@test.com',
            'name' => 'Test Test',
            'password' => Hash::make('test'),
            'created_at' => new \DateTime('now'),
            'updated_at' => new \DateTime('now'),
        ]);
    }
}
