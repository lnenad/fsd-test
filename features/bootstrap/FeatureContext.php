<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context, SnippetAcceptingContext
{
    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     */
    public function __construct()
    {
        $this->ch = curl_init();

        // Initialize your context here
        $this->baseUrl = 'http://localhost/fsdtask/public/';
        $this->url = '';
        $this->method = '';
        $this->responseCode = '';
        $this->siteContents = '';
        $this->authToken = '';
        $this->fields = [];
        $this->data = '';
        $this->retrieved = [];
        $this->performedCurl = false;
    }

    /**
     * @When /^I go to "([^"]*)"$/
     */
    public function iGoTo($arg1)
    {
        $this->baseUrl = $arg1;
    }


    /**
     * @Given /^I request "([^"]*)" with "([^"]*)" using "([^"]*)"$/
     */
    public function iRequestWith($arg1, $arg2, $arg3)
    {
        if (isset($this->retrieved[$arg2])) {
            $this->iRequest($arg1. $this->retrieved[$arg2], $arg3);
        } else {
            throw new Exception(
                "The: " . $arg2." variable is not set\n"
            );
        }
    }

    /**
     * @When /^I request "([^"]*)" using "([^"]*)"$/
     */
    public function iRequest($arg, $arg2)
    {
        $this->url = trim($this->baseUrl.$arg);
        $this->method = $arg2;

        // If a request has already been performed in a scenario
        // reset curl options for this request
        if ($this->performedCurl) {
            curl_reset($this->ch);
        }

        // Set curl options
        $this->setCurlOptions();

        // Retrieve response and populate variables
        $response = curl_exec($this->ch);
        $info = curl_getinfo($this->ch);
        $this->responseCode = $info['http_code'];
        $this->siteContents = $response;

        // Mark in case another request is supposed to be
        // performed in the scenario
        $this->performedCurl = true;
    }

    /**
     * Internal function used to set curl options
     * depending on the request method
     *
     * @return bool
     */
    private function setCurlOptions()
    {
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $this->authToken,
        ));

        if ($this->method == 'POST') {
            $fieldsString = '';
            foreach($this->fields as $key=>$value) { $fieldsString .= $key.'='.$value.'&'; }
            rtrim($fieldsString, '&');

            curl_setopt($this->ch, CURLOPT_POST, count($this->fields));
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $fieldsString);
        } else if ($this->method == 'PUT') {
            $this->fields['_method'] = 'PUT';

            $fieldsString = '';
            foreach($this->fields as $key=>$value) { $fieldsString .= $key.'='.$value.'&'; }
            rtrim($fieldsString, '&');

            curl_setopt($this->ch, CURLOPT_POST, count($this->fields));
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $fieldsString);
        }

        return true;
    }

    /**
     * @Then /^I get a "([^"]*)" status code/
     */
    public function iGetAStatusCode($code)
    {
        if ($this->responseCode == $code) {
            return true;
        } else {
            throw new Exception(
                "Actual code is:\n" . $this->responseCode."\n".
                "Error:\n" . $this->siteContents
            );
        }
    }

    /**
     * @Given /^I read a "([^"]*)" response$/
     */
    public function iReadAResponse($needle)
    {
        $siteJson = json_decode($this->siteContents, true);

        $this->data = $data = $siteJson['data'];

        foreach ($data as $key=>$value) {
            if ($key == $needle) {
                return true;
            }
        }

        foreach ($data as $key=>$value) {
            if (is_array($value)) {
                foreach ($value as $key2 => $value2) {
                    if ($key2 == $needle) {
                        return true;
                    }
                }
            }
        }

        throw new Exception(
            "Not found in :\n" . print_r($data, true)
        );
    }


    /**
     * @Given /^I use auth token "([^"]*)"$/
     */
    public function iUseAuthToken($arg1)
    {
        $this->authToken = $arg1;

        return true;
    }

    /**
     * @Given /^I end$/
     */
    public function iEnd()
    {
        curl_close($this->ch);

        return true;
    }

    /**
     * @Given /^I retrieve first "([^"]*)"$/
     */
    public function iRetrieveFirst($arg1)
    {
        foreach ($this->data as $key=>$value) {
            if ($key === $arg1) {
                $this->retrieved[$key] = $value;
                return true;
            }
        }

        foreach ($this->data as $key=>$value) {
            if (is_array($value)) {
                foreach ($value as $key2 => $value2) {
                    if ($key2 === $arg1) {
                        $this->retrieved[$key2] = $value2;
                        return true;
                    }
                }
            }
        }

        foreach ($this->data as $key=>$value) {
            if (is_array($value)) {
                foreach ($value as $key2 => $value2) {
                    if (is_array($value2)) {
                        foreach ($value2 as $key3 => $value3) {
                            if ($key3 === $arg1) {
                                $this->retrieved[$key3] = $value3;
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * @When /^I set "([^"]*)" to "([^"]*)"$/
     */
    public function iSetTo($arg1, $arg2)
    {
        if ($arg2 == 'random') {
            $this->fields[$arg1] = substr(str_shuffle('abcdefghijklABCDIJKL'), 0, 7);
        } else if ($arg2 == 'randomEmail') {
            $this->fields[$arg1] = substr(str_shuffle('abcdefghijklABCDIJKL'), 0, 7).'@behat.com';
        } else {
            $this->fields[$arg1] = $arg2;
        }
    }
}
