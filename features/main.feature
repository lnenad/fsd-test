Feature: Basic functionality
  Login, book retrieval and authorization check

  #Should perform login procedure
  Scenario: Check login
    When I set "email" to "test@test.com"
    When I set "password" to "test"
    And I request "korisnici/login" using "POST"
    Then I get a "200" status code
    And I read a "success" response
    And I end

  #Should retrieve all books
  Scenario: Get all books
    And I request "knjige" using "GET"
    Then I get a "200" status code
    And I read a "books" response
    And I end

  #Should return 401 unauthorized
  Scenario: Try to add a book
    And I request "knjige" using "POST"
    Then I get a "401" status code
    And I end
