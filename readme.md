# FSD Test API | Nenad Lukic

This is the API made for interview test for FSD. 

It is built in Laravel 5.2 and ticks all of the boxes in the requirements file. 

## Entities

It features User | Author | Book entities with their respective controllers

## Deployment

To successfully deploy this project on a local machine please perform the following commands in the Bash/CP window.

```
cp .env.example .env
```
After doing that please enter database information in the .env file and run


```
composer install
composer update
php artisan key:generate
php artisan migrate
php artisan db:seed
```
Your installation should be complete.
## Usage

To use the resources available your API requests should be targeting the following endpoints

| Method | Endpoint | Description | Request Params |
|-------|-------|--------|-------|
| **GET** | knjige | Retrieves all of the resources | offset |
| **GET** | knjige/pretraga | Searches for a resource using book title or author name | offset |
| **POST** | knjige | Adds a new resource | title, year, language, original_language, author_id |
| **PUT** | knjige/{id} | Updates a resource entry using an ID identifier | title, year, language, original_language, author_id |
| **DELETE** | knjige/{id} | Deletes a resource entry using an ID identifier | / |
| **GET** | autori | Retrieves all of the resources | offset |
| **POST** | autori | Adds a new resource | name |
| **PUT** | autori/{id} | Updates a resource entry using an ID identifier | name |
| **DELETE** | autori/{id} | Deletes a resource entry using an ID identifier | / |
| **POST** | korisnici | Performs user registration | name, email, password |
| **POST** | korisnici/logout | Performs logout procedure | / |
| **POST** | korisnici/login | Performs login procedure | email, password |
___
Responses are encoded in JSON and are wrapped in the data object. Along with data, where available pagination object will be passed with nextOffset attribute. The nextOffset attribute is responsible for providing the offset for accessing the next page of the resource. The entities are retrieved with their relationships

Example of a Book entity response
```
{
      "id": 1,
      "title": "Ledeno Doba",
      "author_id": "1",
      "year": "2016-02-11",
      "language": "SR",
      "original_language": "EN",
      "author": {
        "id": 1,
        "name": "Dzosua Dzordz",
        "created_at": "2016-02-16 14:38:16",
        "updated_at": "2016-02-16 14:38:16"
  }
},
```

Example of an Author entity response
```
{
      "id": 2,
      "name": "Stiven King",
      "created_at": "2016-02-16 14:38:16",
      "updated_at": "2016-02-16 14:38:16",
      "books": [
        {
          "id": 3,
          "title": "Mracna Kula",
          "author_id": "2",
          "year": "2015-06-21",
          "language": "SR",
          "original_language": "EN",
          "created_at": "2016-02-16 14:38:15",
          "updated_at": "2016-02-16 14:38:15"
        },
        {
          "id": 4,
          "title": "Talisman",
          "author_id": "2",
          "year": "2015-08-30",
          "language": "SR",
          "original_language": "EN",
          "created_at": "2016-02-16 14:38:15",
          "updated_at": "2016-02-16 14:38:15"
        }
    ]
},
```

## Unit tests

There are tests that are meant to test resource retrieval, login procedure and authorization. To run them change the baseUrl variable in the features/bootstrap/FeatureContext.php file, and then input the following code into your Bash/CP window
```
vendor/bin/behat
```

___
For additional info contact me at nenadspp@gmail.com