<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded = [];

    /**
     * One book belongs to an author
     *
     * @return \App\Book
     */
    public function author()
    {
        return $this->belongsTo('App\Author');
    }

    /**
     * Scope to join authors to their respective books
     *
     * @param $query
     * @return mixed
     */
    public function scopeJoinAuthor($query)
    {
        return $query->leftJoin('authors', 'books.author_id', '=', 'authors.id');
    }

    /**
     * Scope used to perform search matching with book title or author name
     *
     * @param $query
     * @param $parameter
     * @return mixed
     */
    public function scopeSearch($query, $parameter)
    {
        return $query->where(function ($query) use ($parameter) {
            $query->where('books.title', 'LIKE', '%'.$parameter.'%')->orWhere('authors.name', 'LIKE', '%'.$parameter.'%');
        });
    }

    /**
     * Selector scope | Selects all necessary fields and if needed can be used to add global filters
     *
     * @param $query
     */
    public function scopeSelector($query)
    {
        $query->select(['books.id', 'books.title', 'books.author_id', 'books.year', 'books.language', 'books.original_language']);
    }

    /**
     * Method to eager load and assign authors to books
     * where needed
     *
     * @param $books
     * @return mixed
     */
    public static function addAuthors($books)
    {
        $ids = [];
        // Get all author ids
        foreach ($books as $book) {
            $ids[] = $book->author_id;
        }
        // Eager load all authors
        $authors = Author::whereIn('id', $ids)->get();

        // Assign author objects to the book object
        foreach ($books as $book) {
            $book->author = $authors->where('id', (int)$book->author_id);    // Extract the correct author from the collection and assign it to the book
        }

        return $books;
    }
}
