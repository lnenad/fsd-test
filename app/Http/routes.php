<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => ['web']], function () {

    /*
    |--------------------------------------------------------------------------
    | Secure Routes
    |--------------------------------------------------------------------------
    |
    | This route group applies the "auth" middleware group to every route
    | it contains. The "auth" middleware group created to authenticate users
    | to separate guests from registered users.
    |
    */

    Route::group(['middleware' => ['auth']], function () {
        Route::post('knjige', ['as' => 'books.new', 'uses' => 'BookController@create']);
        Route::put('knjige/{book}', ['as' => 'books.edit', 'uses' => 'BookController@update']);
        Route::delete('knjige/{book}', ['as' => 'books.delete', 'uses' => 'BookController@delete']);

        Route::post('autori', ['as' => 'author.new', 'uses' => 'AuthorController@create']);
        Route::put('autori/{author}', ['as' => 'author.edit', 'uses' => 'AuthorController@update']);
        Route::delete('autori/{author}', ['as' => 'author.delete', 'uses' => 'AuthorController@delete']);

        Route::post('korisnici/logout', ['as' => 'users.logout', 'uses' => 'UserController@logout']);
    });


    /*
    |-------------------------------------------------------------------------
    | Insecure Routes
    |-------------------------------------------------------------------------
     */

    Route::get('knjige', ['as' => 'books.all', 'uses' => 'BookController@index']);
    Route::get('autori', ['as' => 'authors.all', 'uses' => 'AuthorController@index']);
    Route::get('knjige/pretraga', ['as' => 'books.search', 'uses' => 'BookController@search']);

    Route::post('korisnici/login', ['as' => 'users.login', 'uses' => 'UserController@login']);
    Route::post('korisnici', ['as' => 'users.signup', 'uses' => 'UserController@signup']);
});