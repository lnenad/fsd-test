<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Basic parent controller responsible for
 * handling all responses.
 *
 * Class ResponseController
 * @package App\Http\Controllers
 */
class ResponseController extends Controller
{
    public function Json($data, $pagination = false, $status = 200)
    {
        return response()->json(['data' => $data, 'pagination' => $pagination], $status);
    }
}
