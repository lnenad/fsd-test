<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Http\Controllers\ResponseController;

/**
 * Controller responsible for handling Books resource
 *
 * Class BookController
 * @package App\Http\Controllers
 */
class BookController extends ResponseController
{
    protected $perPage = 10;

    /**
     * Index endpoint | Returns all books
     *
     * Not protected
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $offset = $request->input('offset', 0);

        $books = Book::selector()->with('author')->take($this->perPage)->skip($offset)->get();
        $totalBooks = Book::all();

        if (count($totalBooks) > $offset + $this->perPage)
            $next = $offset+$this->perPage;
        else
            $next = false;

        $pagination = [
            'nextOffset' => $next,
        ];

        return $this->Json($books, $pagination);
    }

    /**
     * Search endpoint | Returns all books that correspond to the search parameter
     *
     * Searches book title and author
     *
     * Not protected
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        $parameter = $request->input('parameter', false);

        if (!$parameter)
            return $this->Json(['error' => true, 'errorText' => 'Ne postoji parametar za pretragu'], false, 400);

        $offset = $request->input('offset', 0);

        $books = Book::selector()->joinAuthor()->search($parameter)->take($this->perPage)->skip($offset)->get();
        $totalBooks = Book::select(['books.id'])->joinAuthor()->search($parameter)->get();

        // Only try to eager load the authors if there are results
        if (count($books) > 0)
            $books = Book::addAuthors($books);

        if (count($totalBooks) > $offset + $this->perPage)
            $next = $offset+$this->perPage;
        else
            $next = false;

        $pagination = [
            'nextOffset' => $next,
        ];

        return $this->Json($books, $pagination);
    }

    /**
     * Method used to create the resource - book
     *
     * Protected
     *
     * @param request | title
     * @param request | year
     * @param request | language
     * @param request | original_language
     * @param request | author_id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $fields = ['title', 'year', 'language', 'original_language', 'author_id',];
        $validatedInput = $request->only($fields);

        $validator = Validator::make($validatedInput, [
            'title' => 'required',
            'year' => 'required',
            'language' => 'required',
            'original_language' => 'required',
            'author_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->Json(['error' => true, 'errorText' => $validator->messages()], false, 400);
        }

        $validatedInput['created_at'] = new \DateTime('now');
        $validatedInput['updated_at'] = new \DateTime('now');

        $book = Book::create($validatedInput);

        if($book->exists)
            return $this->Json(['success' => true, 'id' => $book->id]);
        else
            return $this->Json(['success' => false]);
    }

    /**
     * Method used to update the resource - book
     *
     * Protected
     *
     * @param request | title
     * @param request | year
     * @param request | language
     * @param request | original_language
     * @param request | author_id
     *
     * @param Book $book
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Book $book, Request $request)
    {
        $fields = ['title', 'year', 'language', 'original_language', 'author_id',];
        $validatedInput = $request->only($fields);

        $validator = Validator::make($validatedInput, [
            'title' => 'required',
            'year' => 'required',
            'language' => 'required',
            'original_language' => 'required',
            'author_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->Json(['error' => true, 'errorText' => $validator->messages()], false, 400);
        }

        $validatedInput['updated_at'] = new \DateTime('now');

        if ($book->update($validatedInput))
            return $this->Json(['success' => true]);
        else
            return $this->Json(['success' => false]);
    }

    /**
     * Method used to delete the resource - book
     *
     * Protected
     *
     * @param Book $book
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Book $book, Request $request)
    {
        if ($book->delete())
            return $this->Json(['success' => true]);
        else
            return $this->Json(['success' => false]);
    }
}
