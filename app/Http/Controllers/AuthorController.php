<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\ResponseController;

use Illuminate\Support\Facades\Validator;

use App\Author;

/**
 * Controller responsible for handling Author resource
 *
 * Class AuthorController
 * @package App\Http\Controllers
 */
class AuthorController extends ResponseController
{
    protected $perPage = 10;

    /**
     * Index endpoint | Returns all books
     *
     * Not protected
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $offset = $request->input('offset', 0);

        $authors = Author::with('books')->take($this->perPage)->skip($offset)->get();
        $totalAuthors = Author::all();

        if (count($totalAuthors) > $offset + $this->perPage)
            $next = $offset+$this->perPage;
        else
            $next = false;

        $pagination = [
            'nextOffset' => $next,
        ];

        return $this->Json($authors, $pagination);
    }


    /**
     * Method used to create the resource - book
     *
     * Protected
     *
     * @param request | title
     * @param request | year
     * @param request | language
     * @param request | original_language
     * @param request | author_id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $fields = ['name',];
        $validatedInput = $request->only($fields);

        $validator = Validator::make($validatedInput, [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->Json(['error' => true, 'errorText' => $validator->messages()], false, 400);
        }

        $validatedInput['created_at'] = new \DateTime('now');
        $validatedInput['updated_at'] = new \DateTime('now');

        $author = Author::create($validatedInput);

        if($author->exists)
            return $this->Json(['success' => true, 'id' => $author->id]);
        else
            return $this->Json(['success' => false]);
    }


    /**
     * Method used to update the resource - author
     *
     * Protected
     *
     * @param request | name
     *
     * @param Author $author
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Author $author, Request $request)
    {
        $fields = ['name',];
        $validatedInput = $request->only($fields);

        $validator = Validator::make($validatedInput, [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->Json(['error' => true, 'errorText' => $validator->messages()], false, 400);
        }

        $validatedInput['updated_at'] = new \DateTime('now');

        if ($author->update($validatedInput))
            return $this->Json(['success' => true]);
        else
            return $this->Json(['success' => false]);
    }

    /**
     * Method used to delete the resource - author
     *
     * @param Author $author
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Author $author, Request $request)
    {
        if ($author->delete())
            return $this->Json(['success' => true]);
        else
            return $this->Json(['success' => false]);
    }
}
