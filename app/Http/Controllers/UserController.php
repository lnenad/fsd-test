<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Http\Requests;
use App\Http\Controllers\ResponseController;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\User;

class UserController extends ResponseController
{

    /**
     * Method used to perform login
     *
     * Not protected
     *
     * @param request | email
     * @param request | password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $fields = ['email', 'password', ];
        $validatedInput = $request->only($fields);

        $validator = Validator::make($validatedInput, [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->Json(['error' => true, 'errorText' => $validator->messages()], false, 400);
        }

        $email = $request->input('email', false);
        $password = $request->input('password', false);

        $user = User::whereEmail($email)->first();
        if ($user) {
            if (Hash::check($password, $user->password)) {
                Auth::login($user);
            } else {
                return $this->Json(['error' => true, 'errorText' => 'Invalid credentials'], false, 401);
            }
        } else {
            return $this->Json(['error' => true, 'errorText' => 'Invalid credentials'], false, 401);
        }

        return $this->Json(['success' => true]);
    }

    /**
     * Method used to create the resource - book
     *
     * Protected
     *
     * @param request | name
     * @param request | email
     * @param request | password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signup(Request $request)
    {
        $fields = ['name', 'email', 'password', ];
        $validatedInput = $request->only($fields);

        $validator = Validator::make($validatedInput, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->Json(['error' => true, 'errorText' => $validator->messages()], false, 400);
        }

        $validatedInput['created_at'] = new \DateTime('now');
        $validatedInput['updated_at'] = new \DateTime('now');

        $user = User::create($validatedInput);

        if($user->exists)
            return $this->Json(['success' => true, 'id' => $user->id]);
        else
            return $this->Json(['success' => false]);
    }

    /**
     * Method used to perform logout
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        if (Auth::check()) {
            Auth::logout();
            return $this->Json(['success' => true]);
        } else {
            return $this->Json(['success' => false]);
        }
    }
}
