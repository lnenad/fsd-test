<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $guarded = [];

    /**
     * One author has many books
     *
     * @return \App\Author
     */
    public function books()
    {
        return $this->hasMany('App\Book');
    }
}
